#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "homes.h"


//  Write a function called readhomes that takes the filename
// of the homes file and returns an array of pointers to home structs
// terminated with a NULL
char **readhomes ( char *filename )
{
    FILE *file = fopen(filename,"r");
    
    home *homeArray[9977];
    int count = 1;
    int zip;
    char addr[30];
    int price;
    int area;
    // Read CSV file from standard input
    
    if( file == NULL )
    {
       printf("Error while opening the file.\n");
       exit(1);
    }
    
    
    while ( fscanf(file, "%i, %[^,],%i,%i", &zip, addr, &price, &area) != EOF )
    { 
        // Allocate space for a struct home
        home *h = (home *)malloc(sizeof(home));
        
        // Copy values into the struct
        h->zip = zip;
        strcpy( h->addr, addr );
        h->price = price;
        h->area = area;
        
        // Add it to the array
        homeArray[count] = h;
        count++;
    }
    
    
   count++;
   homeArray[0]  = count - 2;
   homeArray[count] = NULL;
   fclose(file);
   return homeArray;
    
}



int main(int argc, char *argv[])
{
    if (argc < 1) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    // Print out the array of structs
    home *house = NULL;
    home **houses = readhomes(argv[1]);
    int numOfHouses = (int)houses[0];
    printf("%i houses\n", numOfHouses);
    
    for ( int i = 1; i <= numOfHouses; i++ )
    {
        printf("%i %s %i %i\n", houses[i]->zip,
                                houses[i]->addr, 
                                houses[i]->price, 
                                houses[i]->area); 
    }
    printf("There are %i houses.\n\n", numOfHouses);
    
    
    // Prompt the user to type in a zip code.
    int zip = 0;
    printf("Enter a zip code: ");
    scanf("%i", &zip);
    int zipCount = 0;
    
    for(int i = 1; i <= numOfHouses; i++)
    {
        if(zip == houses[i]->zip)
        {
            printf("%i %s\n", houses[i]->zip,
                                    houses[i]->addr);
        zipCount++;
        }
    }
    printf("There are %i houses in your desired zip code\n\n", zipCount);
    
    // Prompt the user to type in a price.
    int price = 0;
    printf("Enter a the price you want to pay: ");
    scanf("%i", &price);
    
    int priceCount = 0;
    for(int i = 1; i <= numOfHouses; i++)
    { 
        if(price >= houses[i]->price - 5000 && price <= houses[i]->price + 5000)
        {
            printf("%s %i\n", houses[i]->addr, 
                              houses[i]->price);
        priceCount++;
        }
        
    }
    printf("%i house/s in this price range.\n\n", priceCount);
    
    exit(1);
}
